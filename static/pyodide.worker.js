self.languagePluginUrl = './pyodide/'; // Global variable from Pyodide used to configure the URL to download plugins.
importScripts('pyodide/pyodide.js');
importScripts(
    'https://unpkg.com/promise-worker/dist/promise-worker.register.js'
);

/**
 * Wraps the user's code to redirect output from the Python script which would otherwise be shown in the browser's
 * JavaScript console.
 *
 * When the wrapped Python code is executed, the return value from the script is the redirected system output.
 */
const wrapUserCode = userPythonCode => `
import io
import sys
sys.stdout = io.StringIO()
sys.stderr = io.StringIO()
${userPythonCode.trim()}
sys.stdout.getvalue()
`;

registerPromiseWorker(async event => {
    switch (event.type) {
        // Runs Python code on a separate thread using the Web Worker API.
        case 'RUN_CODE':
            const { code } = event;
            try {
                const output = await self.pyodide.runPythonAsync(
                    wrapUserCode(code),
                    () => {}
                );
                return { result: output, isError: false };
            } catch (e) {
                return { result: e.message, isError: true };
            }
        case 'GET_LOADER_PROGRESS':
            return languagePluginLoader;
        default:
            throw new Error(
                `Unhandled event in Pyodide (Python) worker: ${event}`
            );
    }
});
