import React from 'react';
import { Provider } from 'react-redux';
import { withPrefix } from 'gatsby';

import KernelWorker from './src/kernel-worker';
import configureStore from './src/state/store';

export default ({ element }) => {
    const context = {
        kernels: {
            python: {
                worker: new KernelWorker(withPrefix('pyodide.worker.js')),
                name: 'Python',
            },
        },
        isBrowser: typeof navigator !== 'undefined',
    };
    const store = configureStore(context);
    return <Provider store={store}>{element}</Provider>;
};
