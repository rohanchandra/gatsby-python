const console = (
    state = {
        output: [],
    },
    action
) => {
    switch (action.type) {
        case 'CONSOLE_MESSAGE':
            const { message } = action;
            return {
                ...state,
                output: [...state.output, message],
            };
        case 'CONSOLE_MESSAGES':
            const { messages } = action;
            return {
                ...state,
                output: [...state.output, ...messages],
            };
        default:
            return state;
    }
};

export default console;
