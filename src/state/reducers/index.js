import { combineReducers } from 'redux';

import console from './console';
import kernel from './kernel';

const app = combineReducers({
    console,
    kernel,
});

export default app;
