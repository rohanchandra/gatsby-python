const kernel = (
    state = {
        isBusy: false,
    },
    action
) => {
    switch (action.type) {
        case 'KERNEL_BUSY':
            return {
                ...state,
                isBusy: true,
            };
        case 'KERNEL_IDLE':
            return {
                ...state,
                isBusy: false,
            };
        default:
            return state;
    }
};

export default kernel;
