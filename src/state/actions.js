export const setKernelToBusy = () => ({
    type: 'KERNEL_BUSY',
});

export const setKernelToIdle = () => ({
    type: 'KERNEL_IDLE',
});

export const addConsoleMessage = message => addConsoleMessages([message]);

export const addConsoleMessages = messages => ({
    type: 'CONSOLE_MESSAGES',
    messages,
});

export const evaluateCode = (code, language = 'python') => {
    return {
        type: 'EVALUATE_CODE',
        code,
        language,
    };
};
