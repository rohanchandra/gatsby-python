import { take, actionChannel, put, call, getContext } from 'redux-saga/effects';

import {
    addConsoleMessage,
    setKernelToBusy,
    setKernelToIdle,
} from '../actions';

function* getLanguageDataFromSagaContext(languageId) {
    const kernels = yield getContext('kernels');
    if (!kernels.hasOwnProperty(languageId)) {
        throw new Error(
            `No kernel for '${languageId}'. Only '${Object.keys(
                kernels
            )}' are loaded.`
        );
    }
    return kernels[languageId];
}

function* loadLanguageKernel(languageId) {
    const { name, worker } = yield* getLanguageDataFromSagaContext(languageId);
    yield put(addConsoleMessage(`Downloading ${name} ...`));
    yield call([worker, 'load']);
    yield put(addConsoleMessage(`${name} is ready.`));
}

function* evaluateCode(code, languageId) {
    const languageData = yield* getLanguageDataFromSagaContext(languageId);
    const { worker } = languageData;

    const isLoaded = yield call([worker, 'isLoaded']);
    if (!isLoaded) {
        yield call(loadLanguageKernel, languageId);
    }
    const { result } = yield call([worker, 'execute'], code);

    yield put(addConsoleMessage(result));
}

export function* watchEvaluateCodeQueue() {
    // An action channel buffers code evaluation actions into a queue.
    const evaluateCodeQueueChannel = yield actionChannel('EVALUATE_CODE');
    while (true) {
        const { code, language } = yield take(evaluateCodeQueueChannel);
        yield put(setKernelToBusy());
        yield call(evaluateCode, code, language);
        yield put(setKernelToIdle());
    }
}
