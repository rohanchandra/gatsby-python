import { all } from 'redux-saga/effects';

import { watchEvaluateCodeQueue } from './evaluate-queue-saga';

export default function* rootSaga() {
    yield all([watchEvaluateCodeQueue()]);
}
