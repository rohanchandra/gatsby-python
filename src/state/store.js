import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import app from './reducers';
import rootSaga from './sagas';

const configureSagaMiddleware = context => createSagaMiddleware({ context });

const configureStore = context => {
    const sagaMiddleware = configureSagaMiddleware(context);
    const store = createStore(app, applyMiddleware(sagaMiddleware));
    sagaMiddleware.run(rootSaga);
    return store;
};

export default configureStore;
