import PromiseWorker from 'promise-worker';

class KernelWorker {
    // Worker is lazily loaded. Call the load method to initialize the worker.
    constructor(sourceUrl) {
        this._sourceUrl = sourceUrl;
        this._worker = null;
    }

    load() {
        const rawWorker = new Worker(this._sourceUrl);
        this._worker = new PromiseWorker(rawWorker);
        return this._worker.postMessage({ type: 'GET_LOADER_PROGRESS' });
    }

    isLoaded() {
        return this._worker !== null;
    }

    execute(code) {
        if (this._worker === null) {
            throw new Error(
                'KernelWorker not loaded. KernelWorker must be loaded before it can run code.'
            );
        }
        return this._worker.postMessage({ type: 'RUN_CODE', code });
    }
}

export default KernelWorker;
