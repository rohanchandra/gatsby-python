import React from 'react';
import { useStaticQuery, Link, graphql } from 'gatsby';
import { MDXProvider } from '@mdx-js/react';

import ConnectedConsole from './console/connected-console';
import MDXComponents from './mdx';
import './layout.css';

const ListLink = props => (
    <li style={{ display: 'inline-block', marginRight: '1rem' }}>
        <Link to={props.to}>{props.children}</Link>
    </li>
);

export default ({ children }) => {
    const data = useStaticQuery(
        graphql`
            query {
                site {
                    siteMetadata {
                        title
                    }
                }
            }
        `
    );

    return (
        <MDXProvider components={MDXComponents}>
            <div
                style={{
                    margin: '3rem auto',
                    maxWidth: 650,
                    padding: '0 1rem',
                }}
            >
                <header style={{ marginBottom: '1.5rem' }}>
                    <Link
                        to="/"
                        style={{ textShadow: 'none', backgroundImage: 'none' }}
                    >
                        <h3 style={{ display: 'inline' }}>
                            {data.site.siteMetadata.title}
                        </h3>
                    </Link>

                    <ul style={{ listStyle: 'none', float: 'right' }}>
                        <ListLink to="/about/">About</ListLink>
                        <ListLink to="/pyodide/">Pyodide</ListLink>
                        <ListLink to="/chapter-one/">Ch 1</ListLink>
                    </ul>
                </header>

                <main>{children}</main>

                <ConnectedConsole></ConnectedConsole>

                <footer className="siteFooter">
                    © {new Date().getFullYear()}
                </footer>
            </div>
        </MDXProvider>
    );
};
