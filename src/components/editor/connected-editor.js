import { connect } from 'react-redux';

import Editor from './editor';
import { evaluateCode } from '../../state/actions';

const mapStateToProps = (state, props) => {
    return {
        language: props.language,
        initialValue: props.initialValue,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onRunCode: code => {
            dispatch(evaluateCode(code));
        },
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Editor);
