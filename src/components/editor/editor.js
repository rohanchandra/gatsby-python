import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { UnControlled as CodeMirror } from 'react-codemirror2';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/idea.css';
import './styles.css';

// CodeMirror's language mode is only supported client-side.
if (typeof navigator !== 'undefined') {
    require('codemirror/mode/python/python');
}

class Editor extends Component {
    constructor(props) {
        super(props);
        this.instance = null;
    }

    render() {
        const { onRunCode, language, initialValue } = this.props;

        return (
            <div>
                <CodeMirror
                    editorDidMount={editor => {
                        this.instance = editor;
                    }}
                    value={initialValue.trim()}
                    options={{
                        mode: language,
                        theme: 'idea',
                    }}
                />
                <button onClick={() => onRunCode(this.instance.getValue())}>
                    Run code
                </button>
            </div>
        );
    }
}

Editor.propTypes = {
    initialValue: PropTypes.string.isRequired,
    onRunCode: PropTypes.func.isRequired,
    language: PropTypes.string.isRequired,
};

export default Editor;
