import CodeBlock from './code-block';

/**
 * Links markdown tags to MDX components.
 */
export default {
    code: CodeBlock,
};
