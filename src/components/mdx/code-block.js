import React from 'react';
import ConnectedEditor from '../editor/connected-editor';
import PropTypes from 'prop-types';

const CodeBlock = ({ children, className }) => {
    const language = className.replace(/language-/, '');
    return (
        <ConnectedEditor
            initialValue={children}
            language={language}
        ></ConnectedEditor>
    );
};

CodeBlock.propTypes = {
    children: PropTypes.string.isRequired, // The code to render.
    className: PropTypes.string.isRequired, // Contains the language of the code.
};

export default CodeBlock;
