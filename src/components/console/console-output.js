import React, { useEffect, useRef } from 'react';
import consoleOutputStyles from './console-output.module.css';
import PropTypes from 'prop-types';

const ConsoleOutput = ({ output }) => {
    const outputRef = useRef(null);

    const scrollToBottom = () => {
        outputRef.current.scrollTop = outputRef.current.scrollHeight;
    };

    useEffect(scrollToBottom, [output]);

    return (
        <div ref={outputRef} id={consoleOutputStyles.consoleOutput}>
            {output}
        </div>
    );
};

ConsoleOutput.propTypes = {
    output: PropTypes.string.isRequired,
};

export default ConsoleOutput;
