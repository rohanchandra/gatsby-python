import { connect } from 'react-redux';

import ConsoleOutput from './console-output';

const mapStateToProps = state => {
    return {
        output: state.console.output.join('\n'),
    };
};

const ConnectedConsoleOutput = connect(mapStateToProps)(ConsoleOutput);

export default ConnectedConsoleOutput;
