import React from 'react';
import kernelIndicatorStyles from './kernel-indicator.module.css';
import PropTypes from 'prop-types';

const KernelIndicator = ({ isBusy }) => {
    const textIndicator = isBusy ? (
        <p className={kernelIndicatorStyles.busy}>Busy</p>
    ) : (
        <p className={kernelIndicatorStyles.ready}>Ready</p>
    );

    return (
        <div className={kernelIndicatorStyles.kernelIndicator}>
            {textIndicator}
        </div>
    );
};

KernelIndicator.propTypes = {
    isBusy: PropTypes.bool.isRequired,
};

export default KernelIndicator;
