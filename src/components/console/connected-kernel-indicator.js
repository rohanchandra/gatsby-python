import { connect } from 'react-redux';

import KernelIndicator from './kernel-indicator';

const mapStateToProps = state => {
    return {
        isBusy: state.kernel.isBusy,
    };
};

const ConnectedKernelIndicator = connect(mapStateToProps)(KernelIndicator);

export default ConnectedKernelIndicator;
