import React from 'react';

import ConnectedKernelIndicator from './connected-kernel-indicator';
import ConnectedConsoleOutput from './connected-console-output';
import consoleStyles from './connected-console.module.css';

const ConnectedConsole = () => {
    return (
        <div className={consoleStyles.console}>
            <ConnectedKernelIndicator></ConnectedKernelIndicator>
            <ConnectedConsoleOutput></ConnectedConsoleOutput>
        </div>
    );
};

export default ConnectedConsole;
