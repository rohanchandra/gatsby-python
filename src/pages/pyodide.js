import React from 'react';
import { graphql } from 'gatsby';

import SEO from '../components/seo';
import Layout from '../components/layout';

export default ({ data }) => {
    return (
        <Layout>
            <SEO title="Pyodide" />

            <div>
                <p>
                    This website executes Python code with{' '}
                    <a href="https://github.com/iodide-project/pyodide/">
                        Pyodide
                    </a>
                    .
                </p>

                <p>
                    Pyodide is an experimental way to run Python code in the
                    browser using WebAssembly.
                </p>

                <p>
                    The Pyodide library is available under{' '}
                    <a href="https://github.com/iodide-project/pyodide/blob/master/LICENSE">
                        Mozilla Public License Version 2.0
                    </a>
                    .
                </p>

                <p>
                    A copy of the Pyodide files used on this website is
                    available here:
                </p>

                <table>
                    <thead>
                        <tr>
                            <th>File</th>
                            <th>Size</th>
                            <th>Download</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.allFile.edges.map(({ node }, index) => (
                            <tr key={index}>
                                <td>{node.relativePath}</td>
                                <td>{node.prettySize}</td>
                                <td>
                                    <a href={node.publicURL}>Download</a>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </Layout>
    );
};

export const query = graphql`
    {
        allFile(
            filter: { dir: { regex: "/pyodide/" } }
            sort: { fields: relativePath }
        ) {
            edges {
                node {
                    relativePath
                    prettySize
                    publicURL
                }
            }
        }
    }
`;
