import React from 'react';

import Layout from '../components/layout';
import SEO from '../components/seo';
import ConnectedEditor from '../components/editor/connected-editor';

export default () => (
    <Layout>
        <SEO title="Home" />
        <ConnectedEditor
            language="python"
            initialValue={`# Welcome
print('Hello world!')
print('This is Python.')`}
        ></ConnectedEditor>
    </Layout>
);
