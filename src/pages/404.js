import React from 'react';

import Layout from '../components/layout';
import SEO from '../components/seo';

const NotFoundPage = () => (
    <Layout>
        <SEO title="404 Error" />
        <h1>404 - Not found</h1>
        <p>The requested page was not found.</p>
    </Layout>
);

export default NotFoundPage;
